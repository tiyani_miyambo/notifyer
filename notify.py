import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import time
import requests
import itertools

def querycatalog(MsgType,ProcessName):
    success = False
    sleep =3
    retry=3
    count=0
    while count < retry and success == False:
        try:
            url = 'http://192.168.8.110:5001' + '/api/v1.0/EmailValues/' + MsgType + '/' + ProcessName
            session = requests.Session()
            session.trust_env = False
            apicall = session.get(url)
            api = apicall.json()
            success = True
        except:
            count += 1
            time.sleep(sleep)
            print('Connection to API server failed retry ## {}'.format(count))

    if success == True:
        return {'success':True,'value': api}
    else:
        return {'success':False,'message':'Unable to connect to API server {} after {} attempts'.format(url,count)}


def sendingEmail(HostServer,HostPort,sender,recipient,MsgSubject,MsgBody):

    sleep =3
    retry=3
    count=0
    success = False

    server = smtplib.SMTP(HostServer,HostPort)
    msg = MIMEMultipart()

    username = "tiyani_miyambo@outlook.com"
    password = "Tkny@2020"

    msg['FROM'] = sender
    msg['To'] = recipient
    msg['Subject'] = MsgSubject
    noti = MIMEText(MsgBody,'plain')
    msg.attach(noti)
    
    while count < retry and success == False:

        try:
            print('sending mail to ' + recipient )
            server.ehlo()
            server.starttls()
            server.ehlo()
            server.login(username, password)            
            server.sendmail(sender,recipient,msg.as_string())
            server.close()
            success = True
            print('Email sent successfully')
            
        except:
            count += 1
            time.sleep(sleep)
            print('Connection to server failed retry ## {}'.format(count))
            
    
    if success == True:
        return {'success':True,'message': 'Email was successfully sent'}
    else:
        return {'success':False,'message':'Unable to connect to email server after {} attempts'.format(count)}

def StartFun(MsgType,ProcessName):
    result =querycatalog(MsgType,ProcessName)

    try:
        if result['success'] == True:
            oresults = result['value']
            msgBody = oresults['MessageText']
            msgSubject = oresults['MessageCategory']
            recipient = oresults['UserName']
            Msgto = oresults['FirstName']
            msgBody = msgBody.replace('$$name$$',Msgto)
            msgBody = msgBody.replace('$$Process_Name$$',ProcessName)
            sendingEmail('smtp-mail.outlook.com',587,'tiyani_miyambo@outlook.com',recipient,msgSubject,msgBody)
            return {'success': True,'value':'Email sent successfully'}
        else:
            info= result['message']
            return{'success': False,'value':info}
    except:
        return{'success': False,'value':'Email unsuccessfully sent'}




